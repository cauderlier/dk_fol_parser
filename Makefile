
all:
	ocamlbuild -I src -use-menhir main.native

clean:
	ocamlbuild -clean

tests:
	$(MAKE) -C test all

.phony:	all clean tests
