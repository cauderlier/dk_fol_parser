module O = Options

module Z = Print_zf
module D = Print_dk
module C = Print_coq

let pprint_var fmt x =
  match !O.out_format with
  | O.OutZipper -> Z.pprint_var fmt x
  | O.OutDedukti -> D.pprint_var fmt x
  | O.OutCoq -> C.pprint_var fmt x

let pprint_expr fmt x =
  match !O.out_format with
  | O.OutZipper -> Z.pprint_expr fmt x
  | O.OutDedukti -> D.pprint_expr fmt x
  | O.OutCoq -> C.pprint_expr fmt x

let pprint_statement fmt x =
  match !O.out_format with
  | O.OutZipper -> Z.pprint_statement fmt x
  | O.OutDedukti -> D.pprint_statement fmt x
  | O.OutCoq -> C.pprint_statement fmt x

let pprint_statements fmt x =
  match !O.out_format with
  | O.OutZipper -> Z.pprint_statements fmt x
  | O.OutDedukti -> D.pprint_statements fmt x
  | O.OutCoq -> C.pprint_statements fmt x
