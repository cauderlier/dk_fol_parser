type 'a node = { view : 'a; loc : Location.t; mutable ty : ty option }

and expr_view =
  | Var of string
  | Num of string
  | Let of string * term * term
  | App of term * term list
  | Lam of binding list * term

  | True | False
  | Not of prop
  | And of prop * prop
  | Or of prop * prop
  | Imp of prop * prop
  | Eqv of prop * prop
  | All of binding list * prop
  | Ex of binding list * prop
  | All_type of binding list * prop
  | Ex_type of binding_list * prop
  | Equal of ty * term * term

  | Prop
  | Arrow of ty * ty
  | Pi of binding list * ty

  | Type

and term = expr_view node

and ty = term
and prop = term
and binding = string list * ty option

let simple_binding x ty : binding = ([x], Some ty)

let binding_cons x ty_opt (b : binding list) : binding =
  match b with
  | [] -> [x, ty_opt]
  | (y, ty_opt_y) :: b when ty_opt = ty_opt_y ->
     (x :: y, ty_opt) :: b
  | b -> simple_binding x ty :: b

let mk_ ~loc view = { view; loc=~loc }
let var ~loc s = mk_ ~loc (Var s)
let num ~loc n = mk_ ~loc (Num n)
let rec app ~loc t l =
  match t.view with
  | App f args -> mk_ ~loc (App f (args @ l))
  | t -> mk_ ~loc (App t l)
let rec lam ~loc x ty t =
  match t.view with
  | Lam (b, t) -> mk_ ~loc (Lam (binding_cons x (Some ty) b, t))
  | t -> mk_ ~loc (Lam (simple_binding x ty, t))


type statement_view =
  | TypeAlias of string * ty
  | Decl of string * ty
  | Def of string * binding * ty option * term
  | Rew of binding * term * term
  | Goal of string * prop

type statement = statement_view node
