let rec pred_split_aux p l aux1 aux2 =
  match l with
  | [] -> (aux1, aux2)
  | a :: l when p a -> pred_split_aux p l (a :: aux1) aux2
  | a :: l -> pred_split_aux p l aux1 (a :: aux2)

let pred_split p l =
  let (aux1, aux2) = pred_split_aux p l [] [] in
  (List.rev aux1, List.rev aux2)

let rec filter_map_aux f acc = function
  | [] -> List.rev acc
  | a :: l ->
     begin match f a with
     | None -> filter_map_aux f acc l
     | Some b -> filter_map_aux f (b :: acc) l
     end

let filter_map f l = filter_map_aux f [] l

let rec print_list printer fmt l =
  match l with
  | [] -> ()
  | [a] -> printer fmt a
  | a :: l ->
     Format.fprintf fmt "%a%a"
       printer a (print_list printer) l

let rec print_list_with_spaces printer fmt l =
  match l with
  | [] -> ()
  | [a] -> printer fmt a
  | a :: l ->
     Format.fprintf fmt "%a@ %a"
       printer a (print_list_with_spaces printer) l

let rec print_list_with_commas printer fmt l =
  match l with
  | [] -> ()
  | [a] -> printer fmt a
  | a :: l ->
     Format.fprintf fmt "%a,@ %a"
       printer a (print_list_with_commas printer) l

let rec print_list_with_sep_and_spaces printer ~sep fmt l =
  match l with
  | [] -> ()
  | [a] -> printer fmt a
  | a :: l ->
     Format.fprintf fmt "%a%s@ %a"
       printer a sep (print_list_with_sep_and_spaces ~sep printer) l
