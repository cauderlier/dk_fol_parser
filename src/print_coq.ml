module E = Expr
module S = Statement

let builtin_to_string = function
  | E.True -> "True"
  | E.False -> "False"
  | E.Not -> "not"
  | E.And -> "and"
  | E.Or -> "or"
  | E.Imp -> "->"
  | E.Eqv -> "<->"
  | E.Equal -> "eq"
  | E.Nat -> "'nat'"
  | E.Prop -> "Prop"
  | E.Type -> "Set"
  | E.Kind -> "Kind"

let binder_to_string = function
  | E.Lam -> "fun"
  | E.All -> "forall"
  | E.Ex -> "exists"
  | E.AllType -> "forall"
  | E.ExType -> "exists"
  | E.Pi -> "forall"

let pprint_var fmt x = Format.fprintf fmt "%s" (Var.to_string x)

(* To avoid some parens and ease matching of infix applications we
   translate exprs to a version where application and binders use
   lists and locations and type information are discarded *)

type coq_expr =
  | Var of Var.t
  | Num of string
  | Builtin of E.builtin
  | Let of Var.t * coq_expr * coq_expr
  | App of coq_expr * coq_expr list
  | Bind of E.binder * binding * coq_expr
  | Arrow of coq_expr list * coq_expr

and binding = (Var.t list * coq_expr) list

let var x = Var x
let num n = Num n
let builtin b = Builtin b
let let_ x a b = Let (x, a, b)
let app f arg =
    match f with
    | App (head, first_args) -> App (head, first_args @ [arg])
    | _ -> App (f, [arg])
let add_binding x ty binding =
  match binding with
  | [] -> [[x], ty]             (* Should not happen, bindings should be non-empty *)
  | (xs, ty') :: rest ->
     if ty = ty' then (x :: xs, ty) :: rest
     else ([x], ty) :: binding
let bind b x ty body =
  match body with
  | Bind (b', binding, body') when b = b' ->
     Bind (b, add_binding x ty binding, body')
  | _ -> Bind (b, [[x], ty], body)
let arrow a b =
  match b with
  | Arrow (l, c) -> Arrow (a :: l, c)
  | _ -> Arrow ([a], b)

let rec pprint fmt = function
  | Var x -> Format.fprintf fmt "%a" pprint_var x
  | Num n -> Format.fprintf fmt "%s" n
  | Builtin ((E.True|E.False|E.Nat|E.Prop|E.Type|E.Kind) as b) ->
     Format.fprintf fmt "%s" (builtin_to_string b)
  | Builtin ((E.Not|E.And|E.Or|E.Imp|E.Eqv|E.Equal) as b) ->
     Debug.kprint (-1) "Print Warning: printing builtin %s with bad arity"
       (fun k -> k (builtin_to_string b));
     Format.fprintf fmt "%s" (builtin_to_string b)
  | Let (x, a, b) ->
     Format.fprintf fmt "@[(let %a@ :=@ %a@ in@ %a)@]"
       pprint_var x pprint a pprint b
  | App (Builtin E.Not, [a]) ->
     Format.fprintf fmt "@[(~@ %a)@]"
       pprint a
  | App (Builtin E.And, [a; b]) ->
     Format.fprintf fmt "@[(%a@ /\\@ %a)@]"
       pprint a pprint b
  | App (Builtin E.Or, [a; b]) ->
     Format.fprintf fmt "@[(%a@ \\/@ %a)@]"
       pprint a pprint b
  | App (Builtin E.Imp, [a; b]) ->
     Format.fprintf fmt "@[(%a@ ->@ %a)@]"
       pprint a pprint b
  | App (Builtin E.Eqv, [a; b]) ->
     Format.fprintf fmt "@[(%a@ <->@ %a)@]"
       pprint a pprint b
  | App (Builtin E.Equal, [_; a; b]) ->
     Format.fprintf fmt "@[(%a@ =@ %a)@]"
       pprint a pprint b
  | App (Var is_true, [App (Var basics_equal, [_; a; b])]) when
         Var.to_string is_true = "dk_logic.ebP" &&
           Var.to_string basics_equal = "basics._equal_" ->
     Format.fprintf fmt "@[(%a@ =@ %a)@]"
       pprint a pprint b
  | App (f, l) ->
     Format.fprintf fmt "@[(%a)@]"
       (Utils.print_list_with_spaces pprint) (f :: l)
  | Bind (b, binding, t) ->
     Format.fprintf fmt "@[(%s %a,@ %a)@]"
       (binder_to_string b)
       (Utils.print_list_with_spaces pprint_binding) binding
       pprint t
  | Arrow (l, t) ->
     Format.fprintf fmt "@[(%a ->@ %a)@]"
       (Utils.print_list_with_sep_and_spaces pprint ~sep:" ->") l
       pprint t

and pprint_binding fmt (xs, ty) =
  Format.fprintf fmt "@[(%a : %a)@]"
    (Utils.print_list_with_spaces pprint_var) xs
    pprint ty

let rec expr_to_coq_expr t = match E.view t with
  | E.Var x -> var x
  | E.Num n -> num n
  | E.Builtin b -> Builtin b
  | E.Let (x, a, b) -> let_ x (expr_to_coq_expr a) (expr_to_coq_expr b)
  | E.App (f, a) -> app (expr_to_coq_expr f) (expr_to_coq_expr a)
  | E.Bind (b, x, ty, body) -> bind b x (expr_to_coq_expr ty) (expr_to_coq_expr body)
  | E.Arrow (a, b) -> arrow (expr_to_coq_expr a) (expr_to_coq_expr b)
  | E.Alias (_, ty) -> expr_to_coq_expr ty

let pprint_expr fmt t = pprint fmt (expr_to_coq_expr t)

let print_arg fmt (x, ty) =
  Format.fprintf fmt "(%a : %a)@ " pprint_var x pprint_expr ty

let print_context_decl fmt (x, ty_opt) =
  match ty_opt with
  | None -> pprint_var fmt x
  | Some ty -> Format.fprintf fmt "(%a : %a)" pprint_var x pprint_expr ty

let rewrite_rule_counter = ref 0

let rc () =
  let res = !rewrite_rule_counter in
  incr rewrite_rule_counter;
  res

let pprint_statement fmt s = match S.view s with
  | S.TypeAlias (x, ty) ->
     Format.fprintf fmt "@[<2>Definition (* type alias *) %a : Set :=@ %a.@]@."
       pprint_var x pprint_expr ty
  | S.Decl (x, ty, S.Hyp) ->
     Format.fprintf fmt "@[<2>Hypothesis %a : %a.@]@."
       pprint_var x pprint_expr ty
  | S.Decl (x, ty, _) ->
     Format.fprintf fmt "@[<2>Variable %a :@ %a.@]@."
       pprint_var x pprint_expr ty
  | S.Def (x, bindings, None, body) ->
     Format.fprintf fmt "@[<2>Definition %a@ @[%a@]:=@ %a.@]@."
       pprint_var x (Utils.print_list print_arg) bindings
       pprint_expr body
  | S.Def (x, bindings, Some ty, body) ->
     Format.fprintf fmt "@[<2>Definition %a@ @[%a@]: %a@ :=@ %a.@]@."
       pprint_var x (Utils.print_list print_arg) bindings
       pprint_expr ty pprint_expr body
  | S.Rew (bindings, lhs, rhs) ->
     Format.fprintf fmt "@[<2>Hypothsesis rewrite_rule_%d : forall %a, %a@ =@ %a.@]@."
       (rc ())
       (Utils.print_list_with_spaces print_context_decl) bindings
       pprint_expr lhs pprint_expr rhs
  | S.Goal (g, f) ->
     Format.fprintf fmt "@[<2>Theorem %a :@ %a.@]@."
       pprint_var g pprint_expr f

let pprint_statements fmt = List.iter (pprint_statement fmt)
