module E = Expr

type 'typed args = (Var.t * 'typed Expr.t) list
type 'typed rewrite_context = (Var.t * 'typed Expr.t option) list

type declaration_role = Ty | Hyp | Term

type 'typed view =
  | TypeAlias of Var.t * 'typed E.expr
  | Decl of Var.t * 'typed E.expr * declaration_role
  | Def of Var.t * 'typed args * 'typed E.expr option * 'typed E.expr
  | Rew of 'typed rewrite_context * 'typed E.expr * 'typed E.expr
  | Goal of Var.t * 'typed E.expr

type 'typed statement = Location.t * 'typed view
type 'typed t = 'typed statement

let get_loc = fst
let view = snd

let type_alias ~loc x ty = (loc, TypeAlias (x, ty))
let decl ~loc x ty r = (loc, Decl (x, ty, r))
let def ~loc f vars ty body = (loc, Def (f, vars, ty, body))
let rew ~loc ctx l r = (loc, Rew (ctx, l, r))
let goal ~loc h f = (loc, Goal (h, f))

let expr_opt_eq tyox tyoy =
  match tyox, tyoy with
  | None, None -> true
  | Some tyx, Some tyy -> E.equal tyx tyy
  | None, Some _ | Some _, None -> false

let eq s1 s2 = match view s1, view s2 with
  | TypeAlias (x, tyx), TypeAlias (y, tyy) ->
     Var.eq x y && E.equal tyx tyy
  | Decl (x, tyx, rx), Decl (y, tyy, ry) ->
     Var.eq x y && E.equal tyx tyy && rx = ry
  | Def (x, argsx, tyox, bx), Def (y, argsy, tyoy, by) ->
     (* This is very restrictive as we impose arguments to have the
        same name in both definitions.  Considering function
        definitions as sugar for defining lambda expressions would
        remove this restriction since Expr.equal is alpha
        equivalence. *)
     Var.eq x y &&
       List.for_all2 (fun (x, tyx) (y, tyy) -> Var.eq x y && E.equal tyx tyy) argsx argsy &&
         expr_opt_eq tyox tyoy && E.equal bx by
  | Rew (cx, lx, rx), Rew (cy, ly, ry) ->
     (* Same remark as for the def case *)
     List.for_all2
       (fun (x, tyox) (y, tyoy) -> Var.eq x y && expr_opt_eq tyox tyoy) cx cy &&
       E.equal lx ly && E.equal rx ry
  | Goal (x, fx), Goal (y, fy) ->
     Var.eq x y && E.equal fx fy
  | _ -> false

let free_vars_opt = function
  | None -> Varset.empty
  | Some ty -> E.free_vars ty

let free_vars_context_decl (_, ty) = free_vars_opt ty
let free_vars_context binds = Varset.bigunion (List.map free_vars_context_decl binds)

let free_vars_arg (_, ty) = E.free_vars ty
let free_vars_args args = Varset.bigunion (List.map free_vars_arg args)

let free_vars s = match view s with
  | TypeAlias (_, ty) -> E.free_vars ty
  | Decl (_, ty, _) -> E.free_vars ty
  | Def (_, binds, ty, body) ->
     Varset.minus (Varset.union (free_vars_args binds)
                                (Varset.union (free_vars_opt ty) (E.free_vars body)))
                  (Varset.of_list (List.map fst binds))
  | Rew (context, lhs, rhs) ->
     Varset.union (free_vars_context context)
                  (Varset.union (E.free_vars lhs) (E.free_vars rhs))
  | Goal (_, f) -> E.free_vars f
