module L = Lexing

type t = Dummy | Loc of L.position * L.position

let dummy = Dummy
let make i j = Loc (i, j)

let columns pos = pos.L.pos_cnum - pos.L.pos_bol

let print_loc fmt = function
  | Dummy -> ()
  | Loc (beg_pos, end_pos) ->
     if beg_pos.L.pos_lnum = end_pos.L.pos_lnum then
       Format.fprintf fmt
          "@ on line %d@ from char %d@ to char %d"
          beg_pos.L.pos_lnum (columns beg_pos) (columns end_pos)
     else
       Format.fprintf fmt
          "@ from line %d@ char %d@ to line %d@ char %d"
          beg_pos.L.pos_lnum (columns beg_pos)
          end_pos.L.pos_lnum (columns end_pos)

let combine l1 l2 = match (l1, l1) with
  | Loc (beg_pos, _), Loc (_, end_pos) ->
     Loc (beg_pos, end_pos)
  | Loc _, Dummy | Dummy, Loc _ | Dummy, Dummy -> Dummy
