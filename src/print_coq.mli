val pprint_var : Format.formatter -> Var.t -> unit

val pprint_expr : Format.formatter -> 'typed Expr.t -> unit

val pprint_statement : Format.formatter -> 'typed Statement.t -> unit

val pprint_statements : Format.formatter -> 'typed Statement.t list -> unit
