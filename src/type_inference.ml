module E = Expr
module S = Statement

let global_variable_binding : (Var.t, E.texpr) Hashtbl.t =
  let res = Hashtbl.create 16 in
  List.iter (fun (x, ty, _) -> Hashtbl.add res x ty) Default.default_decls;
  res


(* Mutable ty field is required because we allow untyped declarations
   in rewrite contexts *)
type context = (Var.t * E.texpr option ref) list

let context_add x ty ctx = (x, ref (Some ty)) :: ctx

let find_local_binding ~loc x ctxt f ~unbound ~or_else =
  try
    let tyor = List.assoc x ctxt in
    match !tyor with
    | Some ty ->
       Debug.kprint 5 "Var %a : %a found in local context"
         (fun k -> k Print.pprint_var x Print.pprint_expr ty);
       f ty
    | None -> unbound ~loc x tyor f
  with Not_found -> or_else ()

let unbound_check ty ~loc x tyor f =
  tyor := Some ty;
  f ty

exception InferenceError of Location.t * Var.t
let unbound_infer ~loc x tyor =
  raise (InferenceError (loc, x))

let find_global_binding x (f : E.texpr -> 'a) ~or_else =
  try f (Hashtbl.find global_variable_binding x)
  with Not_found -> or_else ()

exception Unbound_variable of Location.t * Var.t
let extra_declarations : (Var.t * E.texpr) list ref = ref []

let on_new_symbol x ty =
  extra_declarations := (x, ty) :: !extra_declarations;
  Hashtbl.add global_variable_binding x ty

let rec infer context (t : E.uexpr) : E.texpr =
  Debug.kprint 5 "Type inference for %a" (fun k -> k Print.pprint_expr t);
  let loc = E.get_loc t in
  match E.view t with
  | E.Var x ->
     let with_type ty = E.tvar ~loc ~ty x in
     find_local_binding ~loc x context with_type
       ~unbound:unbound_infer
       ~or_else:(fun () ->
         find_global_binding x with_type
           ~or_else:(fun () -> raise (Unbound_variable (loc, x))))
  | E.Num n -> E.tnum ~loc n
  | E.Builtin b -> E.tbuiltin ~loc b
  | E.Let (x, a, b) ->
     let a' = infer context a in
     let b' = infer (context_add x (E.get_ty a') context) b in
     E.tlet ~loc x a' b'
  | E.App (f, a) ->
     let nf = infer context f in
     let na = check context a (E.get_domain nf) in
     E.tapp ~loc nf na
  | E.Bind (b, x, ty, body) ->
     let ty' = check_type_or_kind context ty in
     let new_context = context_add x ty' context in
     let nb = infer new_context body in
     E.tbind ~loc b x ty' nb
  | E.Arrow (a, b) ->
     let na = check_type_or_kind context a in
     let nb = check_type_or_kind context b in
     E.tarrow ~loc na nb
  | E.Alias (x, ty) ->
     let nty = check_type_or_kind context ty in
     E.talias ~loc x nty

and check context t ty =
  Debug.kprint 5 "Type checking for %a : %a" (fun k -> k Print.pprint_expr t Print.pprint_expr ty);
  let loc = E.get_loc t in
  let check_type t ty' : unit =
    if not (E.equal ty ty') then
      raise (E.TypeMismatch { E.tm_loc = loc; E.tm_term = t; E.expected = ty; E.infered = ty'})
  in
  let return res = check_type res (E.get_ty res); res in
  match E.view t with
  | E.Var x ->
     let res = E.tvar ~loc ~ty x in
     let with_type ty = return (E.tvar ~loc ~ty x) in
     find_local_binding ~loc x context with_type
       ~unbound:(unbound_check ty)
       ~or_else:(fun () ->
         find_global_binding x with_type
           ~or_else:(fun () ->
             on_new_symbol x ty;
             res))
  | E.Num n -> return (E.tnum ~loc n)
  | E.Builtin b -> return (E.tbuiltin ~loc b)
  | E.Let (x, a, b) ->
     let a' = infer context a in
     let b' = check (context_add x (E.get_ty a') context) b ty in
     return (E.tlet ~loc x a' b')
  | E.App (f, a) ->
     (* The type of (a) is missing, we can either infer it from the
        domain of f or from a.  The first choice allows f to be typed
        by a dependent product and is hence preferred. *)
     begin try
         let f' = infer context f in
         let a' = check context a (E.get_domain f') in
         return (E.tapp ~loc f' a')
       with _ ->
         let a' = infer context a in
         let f' = check context f (E.tarrow ~loc (E.get_ty a') ty) in
         return (E.tapp ~loc f' a')
     end
  | E.Bind (E.Lam, x, tyx, body) ->
    (* Lambda is the only binder for which the type of the expression
       differs from the type of the body *)
     let tyx' = check_type_or_kind context tyx in
     let new_context = context_add x tyx' context in
     let tyb = match E.view ty with
       | E.Arrow (_, tyb) -> tyb
       | E.Bind (E.Pi, y, tyy, tyb) -> E.rename_expr x y tyb
       | _ -> raise (E.NotAnArrow {E.nar_loc = loc; E.nar_term = None; E.nar_type = ty})
     in
     let body' = check new_context body tyb in
     return (E.tlambda ~loc x tyx' body')
  | E.Bind (b, x, t, body) ->
     let t' = check_type_or_kind context t in
     let new_context = context_add x t' context in
     let nb = check new_context body ty in
     return (E.tbind ~loc b x t' nb)
  | E.Arrow (a, b) ->
     let na = check_type_or_kind context a in
     let nb = check_type_or_kind context b in
     return (E.tarrow ~loc na nb)
  | E.Alias (x, ty) ->
     let nty = check_type_or_kind context ty in
     return (E.talias ~loc x nty)

and check_type_or_kind context t =
  let loc = E.get_loc t in
  match E.view t with
  | E.Builtin E.Type -> E.ttype ~loc
  | _ -> check_type context t

and check_type context t =
  let loc = E.get_loc t in
  check context t (E.ttype ~loc)

and check_prop context t =
  let loc = E.get_loc t in
  check context t (E.tprop ~loc)

let check_type_option context ty_opt =
  match ty_opt with
  | None -> None
  | Some ty -> Some (check_type context ty)

let check_binding context (x, ty_opt) = (x, check_type_option context ty_opt)
let check_bindings context = List.map (check_binding context)

let check_arg context (x, ty) = (x, check_type context ty)
let check_args context = List.map (check_arg context)

let check_ty_opt context tyo = match tyo with
  | None -> None
  | Some ty -> Some (check_type_or_kind context ty)

let check_statement (s : E.untyped S.statement) =
  let loc = S.get_loc s in
  match S.view s with
  | S.TypeAlias (x, ty) ->
     Debug.kprint 4 "Type checking type alias %a" (fun k -> k Print.pprint_statement s);
     S.type_alias ~loc x (check_type [] ty)
  | S.Decl (x, ty, S.Hyp) ->
     Debug.kprint 4 "Type checking declaration %a" (fun k -> k Print.pprint_statement s);
     let ty' = check_prop [] ty in
     Hashtbl.add global_variable_binding x ty';
     S.decl ~loc x ty' S.Hyp
  | S.Decl (x, ty, r) ->
     Debug.kprint 4 "Type checking declaration %a" (fun k -> k Print.pprint_statement s);
     let ty' = check_type_or_kind [] ty in
     Hashtbl.add global_variable_binding x ty';
     S.decl ~loc x ty' r
  | S.Def (x, vars, ty_opt, body) ->
     Debug.kprint 4 "Type checking definition %a" (fun k -> k Print.pprint_statement s);
     (* TODO: uee a fold_left as types might depend on previous args *)
     let new_args = check_args [] vars in
     let context = List.map (fun (x, ty) -> (x, ref (Some ty))) new_args in
     let nty_opt = check_type_option context ty_opt in
     let nbody =
       match nty_opt with
       | None -> infer context body
       | Some ty -> check context body ty
     in
     let ty = E.get_ty nbody in
     Hashtbl.add global_variable_binding x (E.typed_ty_with_params ~loc new_args ty);
     S.def ~loc x new_args (Some ty) nbody
  | S.Rew (r_context, lhs, rhs) ->
     Debug.kprint 4 "Type checking rewrite rule %a" (fun k -> k Print.pprint_statement s);
     (* TODO: uee a fold_left as types might depend on previous args *)
     let context =
       List.map (fun (x, tyo) -> (x, ref (check_ty_opt [] tyo))) r_context
     in
     let new_lhs = infer context lhs in
     let new_rhs = check context rhs (E.get_ty new_lhs) in
     let new_rewrite_context = List.map (fun (x, tyor) -> (x, !tyor)) context in
     S.rew ~loc new_rewrite_context new_lhs new_rhs
  | S.Goal (x, f) ->
     Debug.kprint 4 "Type checking goal %a" (fun k -> k Print.pprint_statement s);
     S.goal ~loc x (check_prop [] f)

let declare_sym (x, ty) =
  let role = match E.view (E.get_ty ty) with
    | E.Builtin E.Prop -> S.Hyp
    | E.Builtin E.Type -> S.Ty
    | _ -> S.Term
  in
  S.decl ~loc:(E.get_loc ty) x ty role

let check_statements statements =
  let mapped = List.map check_statement statements in
  (* For now, we rely on Zipperposition prelude mechanism so we do not
     pass the default statements *)

  (* Default.default_statements @ *)
  List.map declare_sym !extra_declarations @ mapped
