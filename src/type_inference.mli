exception Unbound_variable of Location.t * Var.t
exception InferenceError of Location.t * Var.t

val check_statements : Expr.untyped Statement.t list -> Expr.typed Statement.t list
