(* This module assumes type aliases have been inlined.

   It sorts the remaining statements topologically so that the backend
   can treat them in the given order.

   It also removes duplicated statement. (* TODO *)

   If declarations are missing, the user is warned (* TODO *)

*)

module S = Statement

(* The set of symbols that we already declared:
   true means we have declared it,
   false means we are currently declaring it
 *)
let declared_symbols : (Var.t, bool) Hashtbl.t = Hashtbl.create 16

(* A mapping from symbols to their declaring statement.
   Initialized first *)
let declarations : (Var.t, Expr.untyped S.t) Hashtbl.t = Hashtbl.create 16

(* The input *)
let statements : Expr.untyped S.t list ref = ref []

(* The result *)
let res : Expr.untyped S.t list ref = ref []

let warn_missing_declaration x =
  (* Symbol is missing, warn the user but only once per symbol *)
  if not (Hashtbl.mem declared_symbols x)
  then
    begin
      Debug.warn "Symbol %a is not declared" (fun k -> k Print.pprint_var x);
      Hashtbl.add declared_symbols x true
    end

let second_decl x s =
  let s' = Hashtbl.find declarations x in
  match !Options.duplicated_decl_behaviour, (S.eq s s') with
  | Options.Always_ignore, _
  | Options.Warn_if_different, true ->
     Debug.kprint 5 "Silently ignoring second declaration of %a@."
       (fun k -> k Print.pprint_var x)
  | (Options.Warn_if_different|Options.Always_warn), false ->
     Debug.warn "Symbol %a declared twice with different@.%a@.%a@."
       (fun k -> k Print.pprint_var x
                   Print.pprint_statement s'
                   Print.pprint_statement s)
  | Options.Always_warn, true ->
     Debug.warn "Symbol %a declared twice@."
       (fun k -> k Print.pprint_var x)

exception Err_cyclic of Var.t
let err_cyclic x = raise (Err_cyclic x)

let is_decl s = match S.view s with
  | S.Decl _ -> true
  | _ -> false

let is_def s = match S.view s with
  | S.Def _ -> true
  | _ -> false

let rec do_symbol x =
  try
    Debug.kprint 5 "Doing symbol %a" (fun k -> k Print.pprint_var x);
    do_symbol_and_statement (Hashtbl.find declarations x) x
  with Not_found -> warn_missing_declaration x

and do_symbol_and_statement s x =
  if Varset.mem x (Default.default_symbols) then begin
      Debug.kprint 5 "Skipping default symbol %a"
          (fun k -> k Print.pprint_var x);
      remove_statement s
    end
  else begin

  Debug.kprint 5 "Doing symbol %a and statement %a"
               (fun k -> k Print.pprint_var x Print.pprint_statement s);
  if is_decl s && is_def (Hashtbl.find declarations x) then begin
      Debug.kprint 5 "Ignoring declaration of defined symbol %a"
                   (fun k -> k Print.pprint_var x);
      remove_statement s
    end else begin

  Debug.kprint 5 "Symbol %a declared ? %b"
               (fun k -> k Print.pprint_var x (Hashtbl.mem declared_symbols x));
  if Hashtbl.mem declared_symbols x then begin
      Debug.kprint 5 "Symbol %a completed ? %b"
               (fun k -> k Print.pprint_var x (Hashtbl.find declared_symbols x));
      if Hashtbl.find declared_symbols x then
        begin
          second_decl x s;
          remove_statement s
        end
      else
        err_cyclic x
    end else begin
      Hashtbl.add declared_symbols x false;
      do_statement s;
      Hashtbl.add declared_symbols x true;
    end
    end
    end

and remove_statement s =
  Debug.kprint 5 "Removing statement %a" (fun k -> k Print.pprint_statement s);
  statements := List.filter ((<>) s) !statements

and do_statement s =
  Debug.kprint 5 "Doing statement %a" (fun k -> k Print.pprint_statement s);
  let fv = Varset.minus (S.free_vars s) (Default.default_symbols) in
  List.iter do_symbol (Varset.to_list fv);
  res := s :: !res;
  remove_statement s

let do_statements sts =
  statements := sts;
  res := [];
  (* Initialize the Hashtable declarations *)
  List.iter (fun s ->
      match S.view s with

      (* Ignore declarations of symbols already defined *)
      | S.Decl (x, _, _) when
             Hashtbl.mem declarations x &&
               is_def (Hashtbl.find declarations x) -> ()

      | S.Decl (x, _, _) | S.Def (x, _, _, _) -> Hashtbl.add declarations x s
      | _ -> ()
    ) sts;

  while not (!statements = []) do
    let s = List.hd !statements in
    match S.view s with
    | S.Decl (x, _, _)
    | S.Def (x, _, _, _) -> do_symbol_and_statement s x
    | _ -> do_statement s
  done;
  List.rev !res

let sort l = do_statements l
