type t = string
type var = t
let of_string s = s
let to_string s = s
let eq = (=)

let fresh x l =
  if not (List.mem x l) then x
  else
    begin
      let vari i = x ^ "_" ^ string_of_int i in
      let c = ref 0 in
      while List.mem (vari !c) l do
        incr c
      done;
      vari !c
    end
