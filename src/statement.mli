(* We require that the all arguments are explicitely types in function definitions *)
type 'typed args = (Var.t * 'typed Expr.t) list

(* In rewrite contexts, variables might be untyped and
   typing the left-heand-side should suffice to recover the missing
   type information *)
type 'typed rewrite_context = (Var.t * 'typed Expr.t option) list

type 'typed statement
type 'typed t = 'typed statement
type declaration_role = Ty | Hyp | Term
type 'typed view = private
  | TypeAlias of Var.t * 'typed Expr.t
  | Decl of Var.t * 'typed Expr.t * declaration_role
  | Def of Var.t * 'typed args * 'typed Expr.t option * 'typed Expr.t
  | Rew of 'typed rewrite_context * 'typed Expr.t * 'typed Expr.t
  | Goal of Var.t * 'typed Expr.t

val view : 'typed statement -> 'typed view
val get_loc : 'typed statement -> Location.t

val type_alias : loc:Location.t -> Var.t -> 'typed Expr.expr -> 'typed statement
val decl : loc:Location.t -> Var.t -> 'typed Expr.expr -> declaration_role -> 'typed statement
val def : loc:Location.t -> Var.t -> 'typed args -> 'typed Expr.expr option -> 'typed Expr.expr -> 'typed statement
val rew : loc:Location.t -> 'typed rewrite_context -> 'typed Expr.expr -> 'typed Expr.expr -> 'typed statement
val goal : loc:Location.t -> Var.t -> 'typed Expr.expr -> 'typed statement

val eq : 'typed t -> 'typed t -> bool

val free_vars : 'a t -> Varset.t
