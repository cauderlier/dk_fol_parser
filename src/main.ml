module O = Options

let parse file =
  let input = match file with
    | O.Stdin -> stdin
    | O.File s -> open_in s
  in
  let res = Parse_dk.file Lex_dk.token (Lexing.from_channel input) in
  close_in input; res

let main () =
  try
    (* Getting options *)
    Options.parse ();
    Debug.kprint 0 "Parsing file %a" (fun k -> k O.print_input !O.input_file);
    (* Parsing *)
    let statements = parse !Options.input_file in
    Debug.kprint 1 "%d statements after parsing" (fun k -> k (List.length statements));
    Debug.kprint 2 "Parsed:@.%a" (fun k -> k Print.pprint_statements statements);


    (* Expansion of type aliases *)
    let statements =
      if !Options.remove_type_aliases then
        begin
          Debug.print 0 "Expanding type aliases";
          Type_aliasing.remove_type_aliases_stmts statements
        end
      else statements
    in
    Debug.kprint 1 "%d statements after expansion" (fun k -> k (List.length statements));
    Debug.kprint 2 "Expanded:@.%a" (fun k -> k Print.pprint_statements statements);

    (* Topological Sorting *)
    Debug.print 0 "Sorting statements";
    let statements = Sort_statements.sort statements in
    Debug.kprint 1 "%d statements after sorting" (fun k -> k (List.length statements));
    Debug.kprint 2 "Sorted:@.%a" (fun k -> k Print.pprint_statements statements);

    (* Typing *)
    Debug.print 0 "Type checking";
    let statements = Type_inference.check_statements statements in
    Debug.kprint 1 "%d statements after typing" (fun k -> k (List.length statements));

    (* Pretty Printing *)
    Debug.kprint 0 "Pretty-printing in %s format" (fun k -> k (O.out_lang_to_string  !O.out_format));
    Format.printf "%a" Print.pprint_statements statements
  with
  | O.BadOutLang s ->
     Debug.kprint (-1) "Unrecognized output language %s, available languages: %s"
                    (fun k -> k s O.out_lang_string)
  | Debug.Localized (Debug.Parsing, loc, s) ->
     (* TODO: understand why a direct Format.eprintf does not work *)
     Debug.kprint (-1) "Parsing Error%a: %s" (fun k -> k Location.print_loc loc s);
     exit 1
  | Type_inference.Unbound_variable (loc, x) ->
     Debug.kprint (-1) "Type Error%a: Variable %a was not found in context"
                  (fun k -> k Location.print_loc loc
                              Print.pprint_var x);
     exit 1
  | Type_inference.InferenceError (loc, x) ->
     Debug.kprint (-1) "Type Error%a: Could not infer a type for the bound variable %a"
                  (fun k -> k Location.print_loc loc
                              Print.pprint_var x);
     exit 1
  | Expr.TypeMismatch r ->
     Debug.kprint (-1) "Type Error%a: Term %a, Expected type: %a, Infered type: %a"
                  (fun k -> k Location.print_loc r.Expr.tm_loc
                              Print.pprint_expr r.Expr.tm_term
                              Print.pprint_expr r.Expr.expected
                              Print.pprint_expr r.Expr.infered);
     exit 1
  | Expr.NotAnArrow r ->
     (* TODO: also print nar_term if provided *)
     Debug.kprint (-1) "Type Error%a: This expression has type %a, it cannot be applied"
                  (fun k -> k Location.print_loc r.Expr.nar_loc
                              Print.pprint_expr r.Expr.nar_type);
     exit 1
  | e ->
     Debug.kprint (-1) "Unexpected Error!" (fun k -> k);
     raise e

let _ = main ()
