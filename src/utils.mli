(* fst (pred_split p l) is List.filter p l,
   snd (pred_split p l) is List.filter (fun x -> not (p x)) l

 Both lists are computed simultaneously.

 Tail recursive. *)
val pred_split : ('a -> bool) -> 'a list -> ('a list * 'a list)

val filter_map : ('a -> 'b option) -> 'a list -> 'b list

(* Helper function for printing lists. *)
val print_list :
  (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a list -> unit

(* Helper function for printing space-separated lists. *)
val print_list_with_spaces :
  (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a list -> unit

(* Helper function for printing comma-separated lists. *)
val print_list_with_commas :
  (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a list -> unit

(* Helper function for printing sep-separated lists. *)
val print_list_with_sep_and_spaces :
  (Format.formatter -> 'a -> unit) -> sep:string -> Format.formatter -> 'a list -> unit
