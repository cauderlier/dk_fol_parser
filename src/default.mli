val default_symbols : Varset.t
val default_decls : (Var.t * Expr.texpr * Statement.declaration_role) list
val default_statements : Expr.typed Statement.t list
