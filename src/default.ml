(* Default signature *)

module E = Expr
module S = Statement

let loc = Location.dummy

let ttype = E.ttype ~loc
let arr = E.tarrow ~loc
let rec arr_list l ty = match l with
  | [] -> ty
  | a :: l -> arr a (arr_list l ty)

let prop = E.tprop ~loc
let pi x b = E.tpi ~loc x ttype b

let alpha1_var = Var.of_string "α1"
let alpha2_var = Var.of_string "α2"
let alpha3_var = Var.of_string "α3"
let alpha1 = E.tvar ~loc alpha1_var ~ty:ttype
let alpha2 = E.tvar ~loc alpha2_var ~ty:ttype
let alpha3 = E.tvar ~loc alpha3_var ~ty:ttype

let bool_var = Var.of_string "basics.bool__t"
let prod_var = Var.of_string "dk_tuple.prod"
let int_var = Var.of_string "basics.int__t"
let lst_var = Var.of_string "basics.list__t"

(* The kind associated to a type constructor of arity n *)
let rec type_constructor_kind n = match n with
  | 0 -> ttype
  | n -> arr ttype (type_constructor_kind (n-1))

let default_types = [
    bool_var, 0;
    prod_var, 2;
    int_var, 0;
    Var.of_string "basics.char__t", 0;
    Var.of_string "basics.string__t", 0;
    Var.of_string "basics.float__t", 0;
  ]

let bool1 = E.tvar ~loc bool_var ~ty:ttype
let prod = E.tvar ~loc prod_var ~ty:(type_constructor_kind 2)
let tprod a b = E.tapp ~loc (E.tapp ~loc prod a) b
let lst = E.tvar ~loc lst_var ~ty:(type_constructor_kind 1)
let tlst a = E.tapp ~loc lst a
let int = E.tvar ~loc int_var ~ty:ttype
let nat = E.tbuiltin ~loc E.Nat

let default_decls =
  List.map (fun (t, n) -> (t, type_constructor_kind n, S.Ty)) default_types @
  [
    Var.of_string "basics.true", bool1, S.Term;
    Var.of_string "dk_bool.true", bool1, S.Term;
    Var.of_string "basics.false", bool1, S.Term;
    Var.of_string "dk_bool.false", bool1, S.Term;
    Var.of_string "basics._equal_", pi alpha1_var (arr_list [alpha1; alpha1] bool1), S.Term;
    Var.of_string "basics.syntactic_equal", pi alpha1_var (arr_list [alpha1; alpha1] bool1), S.Term;
    Var.of_string "dk_logic.ebP", arr bool1 prop, S.Term;
    Var.of_string "basics._tilda__tilda_", arr bool1 bool1, S.Term;
    Var.of_string "basics._amper__amper_", arr_list [bool1; bool1] bool1, S.Term;
    Var.of_string "basics._bar__bar_", arr_list [bool1; bool1] bool1, S.Term;
    Var.of_string "basics._bar__lt__gt__bar_", arr_list [bool1; bool1] bool1, S.Term;
    Var.of_string "dk_bool.ite", pi alpha1_var (arr_list [bool1; alpha1; alpha1] alpha1), S.Term;
    Var.of_string "dk_fail.fail", pi alpha1_var alpha1, S.Term;

    Var.of_string "basics.pair", pi alpha1_var (pi alpha2_var (arr_list [alpha1; alpha2] (tprod alpha1 alpha2))), S.Term;
    Var.of_string "dk_tuple.pair", pi alpha1_var (pi alpha2_var (arr_list [alpha1; alpha2] (tprod alpha1 alpha2))), S.Term;
    Var.of_string "basics.fst", pi alpha1_var (pi alpha2_var (arr (tprod alpha1 alpha2) alpha1)), S.Term;
    Var.of_string "basics.snd", pi alpha1_var (pi alpha2_var (arr (tprod alpha1 alpha2) alpha2)), S.Term;
    Var.of_string "dk_tuple.match__pair",
    pi alpha1_var
       (pi alpha2_var
           (pi alpha3_var
               (arr_list [tprod alpha1 alpha2; arr_list [alpha1; alpha2] alpha3; alpha3] alpha3))), S.Term;

    Var.of_string "nil", pi alpha1_var (tlst alpha1), S.Term;
    Var.of_string "cons", pi alpha1_var (arr_list [alpha1; tlst alpha1] (tlst alpha1)), S.Term;
    Var.of_string "basics.match_nil", pi alpha1_var (pi alpha2_var (arr_list [tlst alpha1; alpha2; alpha2] alpha2)), S.Term;

    Var.of_string "basics.match_cons", pi alpha1_var (pi alpha2_var (arr_list [tlst alpha1; arr_list [alpha1; tlst alpha1] alpha2; alpha2] alpha2)), S.Term;

    Var.of_string "dk_int.from_nat", arr nat int, S.Term;
    Var.of_string "dk_builtins.call_by_value", pi alpha1_var (pi alpha2_var (arr (arr alpha1 alpha2) (arr alpha1 alpha2))), S.Term;
  ]

let default_symbols = Varset.of_list (List.map (fun (a, _, _) -> a) default_decls)

let default_statements =
  List.map (fun (x, ty, r) -> S.decl ~loc x ty r) default_decls
