(* Should we remove type aliases? *)
let remove_type_aliases = ref true

(* What to do with duplicated statements? *)
type duplicated_behaviour =
  | Always_ignore     (* Second statement is silently ignored *)
  | Always_warn       (* Second statement is ignored but with a warning *)
  | Warn_if_different (* Only warn in case of ambiguity  *)

let duplicated_decl_behaviour = ref Warn_if_different
let duplicated_def_behaviour = ref Warn_if_different

let debug_level = ref (-1)

type outlang = OutDedukti | OutZipper | OutCoq
let out_format = ref OutZipper
let out_languages = [OutZipper; OutDedukti; OutCoq]
let out_lang_to_string = function
  | OutDedukti -> "Dedukti"
  | OutZipper -> "Zipperposition"
  | OutCoq -> "Coq"

let out_lang_string = String.concat " | " (List.map out_lang_to_string out_languages)

exception BadOutLang of string

let set_output_language s =
  let lang =
    match s with
    | "dk" | "dedukti" | "Dedukti" -> OutDedukti
    | "zf" | "zipperposition" | "Zipperposition" -> OutZipper
    | "v" | "coq" | "Coq" -> OutCoq
    | s -> raise (BadOutLang s)
  in
  out_format := lang

let set b () = b := true
let unset b () = b := false

let argspec = [
    "--remove-type-aliases",
    Arg.Unit (set remove_type_aliases),
    "remove type aliases (this is the default)";

    "--no-remove-type-aliases",
    Arg.Unit (unset remove_type_aliases),
    "keep type aliases";

    "--debug",
    Arg.Int (fun i -> debug_level := i),
    "set verbosity level";

    "--output",
    Arg.String set_output_language,
    "set output language (" ^ out_lang_string ^ ")"
  ]

type input = Stdin | File of string
let print_input fmt = function
  | Stdin -> Format.fprintf fmt "<stdin>"
  | File s -> Format.fprintf fmt "%s" s

let input_file = ref Stdin

let parse () = (* TODO: add a usage string *)
  Arg.parse argspec (fun s -> input_file := File s) ""
