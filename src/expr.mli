type builtin =
  | True | False | Not | And | Or | Imp | Eqv | Equal
  | Nat | Prop | Type | Kind

type binder =
  | Lam | All | Ex | AllType | ExType | Pi

type typed
type untyped

type 'typed expr
type 'typed t = 'typed expr
type uexpr = untyped expr
type texpr = typed expr

type type_mismatch = { tm_loc : Location.t; tm_term : texpr; expected : texpr; infered : texpr}
exception TypeMismatch of type_mismatch

type not_an_arrow = { nar_loc : Location.t; nar_term : texpr option; nar_type : texpr}
exception NotAnArrow of not_an_arrow

type 'typed expr_view = private
  | Var of Var.t
  | Num of string
  | Builtin of builtin
  | Let of Var.t * 'typed expr * 'typed expr
  | App of 'typed expr * 'typed expr
  | Bind of binder * Var.t * 'typed expr * 'typed expr
  | Arrow of 'typed expr * 'typed expr
  | Alias of Var.t * 'typed expr

val view : 'typed expr -> 'typed expr_view
val get_loc : 'typed expr -> Location.t
val get_ty : texpr -> texpr

(* The kind of all types *)
val utype : loc:Location.t -> uexpr
val ttype : loc:Location.t -> texpr

val uvar : loc:Location.t -> Var.t -> uexpr
val tvar : loc:Location.t -> ty:texpr -> Var.t -> texpr

val unat : loc:Location.t -> uexpr
val unum : loc:Location.t -> string -> uexpr
val tnum : loc:Location.t -> string -> texpr
val ulambda :
  loc:Location.t ->
  Var.t -> uexpr -> uexpr -> uexpr
val ulet :
  loc:Location.t ->
  Var.t -> uexpr -> uexpr -> uexpr
val tlet :
  loc:Location.t ->
  Var.t -> texpr -> texpr -> texpr
val uapp : uexpr -> uexpr -> uexpr
val tapp : loc:Location.t -> texpr -> texpr -> texpr
val uapp_list : uexpr -> uexpr list -> uexpr
val utrue : loc:Location.t -> uexpr
val ufalse : loc:Location.t -> uexpr
val unot : loc:Location.t -> uexpr -> uexpr
val uand : loc:Location.t -> uexpr -> uexpr -> uexpr
val uor : loc:Location.t -> uexpr -> uexpr -> uexpr
val uimply : loc:Location.t -> uexpr -> uexpr -> uexpr
val uequiv : loc:Location.t -> uexpr -> uexpr -> uexpr
val uequal : loc:Location.t -> uexpr -> uexpr -> uexpr -> uexpr
val ubind : loc:Location.t -> binder -> Var.t -> uexpr -> uexpr -> uexpr
val tbind : loc:Location.t -> binder -> Var.t -> texpr -> texpr -> texpr
val tlambda : loc:Location.t -> Var.t -> texpr -> texpr -> texpr
val upi : loc:Location.t -> Var.t -> uexpr -> uexpr -> uexpr
val tpi : loc:Location.t -> Var.t -> texpr -> texpr -> texpr
val uforall : loc:Location.t -> Var.t -> uexpr -> uexpr -> uexpr
val uexists : loc:Location.t -> Var.t -> uexpr -> uexpr -> uexpr
val uforall_type : loc:Location.t -> Var.t -> uexpr -> uexpr
val uexists_type : loc:Location.t -> Var.t -> uexpr -> uexpr
val ualias : loc:Location.t -> Var.t -> uexpr -> uexpr
val talias : loc:Location.t -> Var.t -> texpr -> texpr
val uarrow : loc:Location.t -> uexpr -> uexpr -> uexpr
val tarrow : loc:Location.t -> texpr -> texpr -> texpr

val tbuiltin : loc:Location.t -> builtin -> texpr

val forget_types : texpr -> uexpr
val uprop : loc:Location.t -> uexpr
val tprop : loc:Location.t -> texpr

val free_vars : 'typed expr -> Varset.t
val equal : 'typed expr -> 'typed expr -> bool
val get_domain : texpr -> texpr
val rename_expr : Var.t -> Var.t -> texpr -> texpr




(* ty_with_params ~loc [(x1, ty1); ... (xn, tyn)] ty is Π x1 :
   ty1. ... Π xn : tyn. ty

   where Π is replaced by an arrow when the bound variable x is not
   used.  *)
val ty_with_params : loc:Location.t -> (Var.t * 'typed expr) list -> 'typed expr -> 'typed expr
val typed_ty_with_params : loc:Location.t -> (Var.t * typed expr) list -> typed expr -> typed expr
