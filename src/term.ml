module E = Expr
module T = Type

let is_a_term t =
  match E.get_type t with
  | Some ty -> T.is_a_type ty
  | None -> false

type term = E.typed E.expr
type term_view =
  | Var of string
  | Num of string
  | Let of string * term * term
  | App of term * term list
  | Fun of string * T.ty * term

exception Not_a_term of E.typed E.expr

let rec view_term t =
  let err () = raise (Not_a_term t) in
  if not (is_a_term t) then err ();
  let loc = E.get_loc t in
  match E.view t with
  | E.Var s -> Var s
  | E.Num s -> Num s
  | E.Let (x, a, b) -> Let (x, a, b)
  | E.App (f, []) -> view_term f
  | E.App (f, l) -> App (f, l)
  | E.Bind (Lam, x, ty, t) -> Fun (x, ty, t)
  | E.Builtin (True|False|Not|And|Or|Imp|Eqv|Equal|Nat|Prop|Type|Alias|Arrow)
  | E.Bind ((Pi|All|Ex|AllType|ExType), _, _, _) -> err ()
