type t
val empty : t
val singl : Var.t -> t
val mem : Var.t -> t -> bool
val subset : (Var.t -> bool) -> t -> t
val add_in_set : t -> Var.t -> t
val union : t -> t -> t
val bigunion : t list -> t
val remove : Var.t -> t -> t
val intersection : t -> t -> t
val minus : t -> t -> t
val of_list : Var.t list -> t
val to_list : t -> Var.t list
val card : t -> int
