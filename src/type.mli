type ty
type type_view = private
  | TProp
  | TNat
  | TVar of string
  | TArr of ty * ty
  | TPi of string * ty

exception Not_a_type of typed expr
val view_ty : ty -> type_view
