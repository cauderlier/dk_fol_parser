type t
val dummy : t
val make : Lexing.position -> Lexing.position -> t
val print_loc : Format.formatter -> t -> unit
val combine : t -> t -> t
