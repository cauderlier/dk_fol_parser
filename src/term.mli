type term

type term_view = private
  | Var of string
  | Num of string
  | Let of string * term * term
  | App of term * term list
  | Fun of string * ty * term

exception Not_a_term of Expr.typed Expr.expr
val view_term : term -> term_view
