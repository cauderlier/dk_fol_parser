val print : int -> (unit, Format.formatter, unit, unit) format4 -> unit
val kprint :
  int -> ('a, Format.formatter, unit, unit) format4 -> ('a -> unit) -> unit
val warn : ('a, Format.formatter, unit, unit) format4 -> ('a -> unit) -> unit
type step = Lexing | Parsing | TypeAlias | Sort | Typing | Printing
exception Localized of step * Location.t * string
