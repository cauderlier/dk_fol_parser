module E = Expr
module T = Type

let is_prop t = t == prop
let is_a_prop f =
  match f.ty with
  | Some ty -> is_prop ty
  | None -> false

type prop = E.typed E.t

type prop_view =
  | True
  | False
  | Not of prop
  | And of prop * prop
  | Or of prop * prop
  | Imp of prop * prop
  | Eqv of prop * prop
  | Eq of Type.t * Term.t * Term.t
  | All of string * Type.t * prop
  | Ex of string * Type.t * prop
  | AllType of string * prop
  | ExType of string * prop

exception Not_a_prop of typed expr

let view_prop f =
  let err () = raise (Not_a_prop f) in
  if not (is_a_prop f) then err ();
  let loc = f.loc in
  match f.view with
  | E.Builtin True -> True
  | E.Builtin False -> False
  | E.App (n, [f]) ->
     begin match n.view with
     | E.Builtin Not -> Not f
     | _ -> err ()
     end
  | E.App (connective, [f1; f2]) ->
     begin match connective.view with
     | E.Builtin And -> And (f1, f2)
     | E.Builtin Or -> Or (f1, f2)
     | E.Builtin Imp -> Imp (f1, f2)
     | E.Builtin Eqv -> Eqv (f1, f2)
     | _ -> err ()
     end
  | E.App (eq, [ty; a; b]) ->
     begin match eq.view with
     | E.Builtin Equal -> Eq (ty, a, b)
     | _ -> err ()
     end
  | E.Bind (All, x, ty, f) -> All (x, ty, f)
  | E.Bind (Ex, x, ty, f) -> Ex (x, ty, f)
  | E.Bind (AllType, x, tty, f) when is_type tty -> AllType (x, f)
  | E.Bind (ExType, x, tty, f) when is_type tty -> ExType (x, f)
  | E.Var _ | E.Num _ | E.App _ | E.Let _
  | E.Builtin (Not|And|Or|Imp|Eqv|Equal|Nat|Prop|Type|Alias|Arrow)
  | E.Bind ((Lam|Pi|AllType|ExType), _, _, _) -> err ()
