type t
type var = t
val of_string : string -> t
val to_string : t -> string
val eq : t -> t -> bool
val fresh : t -> t list -> t
