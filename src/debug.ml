let kprint level msg k =
  if level <= !Options.debug_level then
    begin
      k
      (Format.kfprintf
         (fun fmt -> Format.fprintf fmt "@.")
         Format.err_formatter msg)
    end

let print level msg = kprint level msg (fun () -> ())

type step = Lexing | Parsing | TypeAlias | Sort | Typing | Printing
exception Localized of step * Location.t * string

let warn msg k = kprint (-1) msg k
