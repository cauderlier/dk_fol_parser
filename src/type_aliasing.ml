module E = Expr
module S = Statement

let type_aliases : (Var.t, E.untyped E.expr) Hashtbl.t = Hashtbl.create 16

let find_alias (s : Var.t) : E.untyped E.expr = Hashtbl.find type_aliases s

let check_capture context ty =
  assert (Varset.empty =
            Varset.intersection
              (Varset.of_list context)
              (E.free_vars ty))

let rec remove_type_aliases context t =
  let loc = E.get_loc t in
  match E.view t with
  | E.Var x when List.mem x context -> t
  | E.Var x ->
     begin try
         let ty = find_alias x in
         check_capture context ty;
         E.ualias ~loc x ty
       with Not_found -> t
     end
  | E.Num _
  | E.Builtin _ -> t
  | E.Let (x, a, b) ->
     E.ulet ~loc x
            (remove_type_aliases context a)
            (remove_type_aliases (x :: context) b)
  | E.App (f, a) ->
     E.uapp
            (remove_type_aliases context f)
            (remove_type_aliases context a)
  | E.Bind (binder, x, ty, t) ->
     E.ubind ~loc binder x
             (remove_type_aliases context ty)
             (remove_type_aliases (x :: context) t)
  | E.Arrow (a, b) ->
     E.uarrow ~loc
             (remove_type_aliases context a)
             (remove_type_aliases context b)
  | E.Alias (x, ty) ->
     E.ualias ~loc x ty

let remove_type_aliases_stmt s =
  let f = remove_type_aliases [] in
  let loc = S.get_loc s in
  match S.view s with
  | S.TypeAlias _ -> assert false
  | S.Decl (s, ty, r) -> S.decl ~loc s (f ty) r
  | S.Def (s, vars, None, t) ->
     let new_vars =
       List.map (fun (x, ty) -> (x, f ty)) vars
     in
     let context = List.map fst vars in
     let t = remove_type_aliases context t in
     S.def ~loc s new_vars None t
  | S.Def (s, vars, Some ty, t) ->
     let new_vars =
       List.map
         (function (x, ty) -> (x, f ty))
         vars
     in
     let context = List.map fst vars in
     let ty = remove_type_aliases context ty in
     let t = remove_type_aliases context t in
     S.def ~loc s new_vars (Some ty) t
  | S.Rew (vars, l, r) ->
     let new_vars =
       List.map
         (function (x, None) -> (x, None)
                 | (x, Some ty) -> (x, Some (f ty)))
         vars
     in
     let context = List.map fst vars in
     let l = remove_type_aliases context l in
     let r = remove_type_aliases context r in
     S.rew ~loc new_vars l r
  | S.Goal (s, g) -> S.goal ~loc s (f g)

let remove_type_aliases_stmts l =
  let (aliases_stmts, regular_stmts) =
    Utils.pred_split (fun s -> match S.view s with S.TypeAlias _ -> true | _ -> false) l
  in
  List.iter
    (fun s -> match S.view s with
     | S.TypeAlias (s, ty) -> Hashtbl.add type_aliases s ty
     | _ -> assert false)
    aliases_stmts;
  List.map remove_type_aliases_stmt regular_stmts
