module E = Expr

type ty = E.typed E.expr

type type_view =
  | Prop
  | Nat
  | Var of string
  | Arr of ty * ty
  | Pi of string * ty

let is_type e = e == E.type_
let is_a_type ty =
  match ty.ty with
  | Some ty -> is_type ty
  | None -> false

exception Not_a_type of typed expr

let rec view_ty ty =
  let err () = raise (Not_a_type ty) in
  if not (is_a_type ty) then err ();
  let loc = ty.loc in
  match ty.view with
  | EVar s -> TVar s
  | EBuiltin Prop -> TProp
  | EBuiltin Nat -> TNat
  | EApp (f, [t1; t2]) ->
     begin match f.view with
     | EBuiltin Arrow -> TArr (t1, t2)
     | EBuiltin Alias -> view_ty t2
     | _ -> err ()
     end
  | EBind (Pi, x, ttype, ty) ->
     if ttype <> type_ then err ();
     TPi (x, ty)
  | EBuiltin (True|False|Not|And|Or|Imp|Eqv|Equal|Type|Alias|Arrow)
  | EBind ((Lam|All|Ex|AllType|ExType), _, _, _)
  | ENum _ | ELet _ | EApp _ -> err ()
