type prop
type t = prop

type prop_view = private
  | True
  | False
  | Not of prop
  | And of prop * prop
  | Or of prop * prop
  | Imp of prop * prop
  | Eqv of prop * prop
  | Eq of Type.t * Term.t * Term.t
  | All of string * Type.t * prop
  | Ex of string * Type.t * prop
  | AllType of string * prop
  | ExType of string * prop

exception Not_a_prop of typed expr
val view_prop : prop -> prop_view
