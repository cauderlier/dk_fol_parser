/*  Copyright 2005 INRIA  */
/*  Copyright 2014 Ali Assaf */
/*  Copyright 2014 Raphaël Cauderlier */
%{
module E = Expr
module L = Location
module S = Statement

let dkparse_error ~loc s = raise (Debug.Localized (Debug.Parsing, loc, s))
%}
%token <string> ID QID NUMBER
%token COLON DOT DOUBLE_ARROW DEF ARROW
%token TYPE TERM PROOF CCARR PROP
%token LPAREN RPAREN EOF
%token LBRACK COMMA RBRACK REW DEFKW
%token TRUE FALSE NOT AND OR IMP EQV ALL EX ALL_TYPE EX_TYPE EQUAL

%token BEGINPROOF
%token BEGIN_TYPEALIAS
%token BEGIN_TY
%token BEGIN_VAR
%token BEGIN_HYP
%token END_TYPEALIAS
%token END_VAR
%token END_HYP
%token <string> BEGINNAME
%token BEGINHEADER
%token ENDPROOF

%start file
%type <Expr.untyped Statement.t list> file
%type <Expr.uexpr> typ
%type <Expr.uexpr> term
%type <Expr.uexpr> term_simple

%%

file:
| b=body EOF { b }
| BEGINPROOF h=proofheader* b=body ENDPROOF EOF { List.flatten h @ b }
| error {
  let loc = L.make $startpos $endpos in
  dkparse_error ~loc "expected problem"
}

proofheader:
  | BEGINNAME { [] }
  | BEGINHEADER { [] }
  | BEGIN_TY id=ID
      {
        let loc = L.make $startpos $endpos in
        [ S.decl ~loc (Var.of_string id) (E.utype ~loc) S.Ty ]
      }
  | BEGIN_TYPEALIAS f=ID DEF ty=type_simple END_TYPEALIAS
      {
        let loc = L.make $startpos $endpos in
        [ S.type_alias ~loc (Var.of_string f) ty ]
      }
  | BEGIN_VAR ID COLON typ END_VAR { [] }
  | BEGIN_HYP ID COLON PROOF term_simple END_HYP { [] }
  | error {
  let loc = L.make $startpos $endpos in
  dkparse_error ~loc "expected proofheader"
}

body:
| s=statement b=body {s :: b}
| g=goal { [g] }

statement:
| id=ID COLON ty=kind DOT
  { let loc = L.make $startpos $endpos in
    S.decl ~loc (Var.of_string id) ty S.Ty }
| id=QID COLON ty=kind DOT
  { let loc = L.make $startpos $endpos in
    S.decl ~loc (Var.of_string id) ty S.Ty }
| name=ID COLON PROOF t=term DOT
  { let loc = L.make $startpos $endpos in
    S.decl ~loc (Var.of_string name) t S.Hyp }
| name=QID COLON PROOF t=term DOT
  { let loc = L.make $startpos $endpos in
    S.decl ~loc (Var.of_string name) t S.Hyp }
| id=ID COLON ty=arrow_type DOT
  { let loc = L.make $startpos $endpos in
    S.decl ~loc (Var.of_string id) ty S.Term }
| id=QID COLON ty=arrow_type DOT
  { let loc = L.make $startpos $endpos in
    S.decl ~loc (Var.of_string id) ty S.Term }
| DEFKW id=ID COLON ty=arrow_type DOT
  { let loc = L.make $startpos $endpos in
    S.decl ~loc (Var.of_string id) ty S.Term }
| DEFKW id=QID COLON ty=arrow_type DOT
  { let loc = L.make $startpos $endpos in
    S.decl ~loc (Var.of_string id) ty S.Term }
| DEFKW id=ID COLON ty=typ DEF body=term DOT
  { let loc = L.make $startpos $endpos in
    S.def ~loc (Var.of_string id) [] (Some ty) body }
| DEFKW id=QID COLON ty=typ DEF body=term DOT
  { let loc = L.make $startpos $endpos in
    S.def ~loc (Var.of_string id) [] (Some ty) body }
| DEFKW id=declared_or_defined_id args=compact_arg+ COLON ty_ret=typ DEF body=term DOT
  { let loc = L.make $startpos $endpos in
    S.def ~loc (Var.of_string id) args (Some ty_ret) body
  }
| env=env lhs=term REW rhs=term DOT
  { let loc = L.make $startpos $endpos in
    S.rew ~loc env lhs rhs
  }

goal:
| name=ID COLON PROOF t=term DOT
  { let loc = L.make $startpos $endpos in
    S.goal ~loc (Var.of_string name) t }

qid:
| x=QID { Var.of_string x }
| x=ID { Var.of_string x }

term_simple:
| x=qid
  { let loc = L.make $startpos $endpos in
    E.uvar ~loc x }
| x=NUMBER
  { let loc = L.make $startpos $endpos in
    E.unum ~loc x }
| TRUE
  { let loc = L.make $startpos $endpos in
    E.utrue ~loc }
| FALSE
  { let loc = L.make $startpos $endpos in
    E.ufalse ~loc }
| NOT t=term_simple
  { let loc = L.make $startpos $endpos in
    E.unot ~loc t }
| AND t=term_simple u=term_simple
  { let loc = L.make $startpos $endpos in
    E.uand ~loc t u }
| OR  t=term_simple u=term_simple
  { let loc = L.make $startpos $endpos in
    E.uor ~loc t u }
| IMP t=term_simple u=term_simple
  { let loc = L.make $startpos $endpos in
    E.uimply ~loc t u }
| EQV t=term_simple u=term_simple
  { let loc = L.make $startpos $endpos in
    E.uequiv ~loc t u }
| ALL ty=type_simple LPAREN x=ID COLON complex_type DOUBLE_ARROW body=term RPAREN
  { let loc = L.make $startpos $endpos in
    E.uforall ~loc (Var.of_string x) ty body }
| EX ty=type_simple LPAREN x=ID COLON complex_type DOUBLE_ARROW body=term RPAREN
  { let loc = L.make $startpos $endpos in
    E.uexists ~loc (Var.of_string x) ty body }
| ALL_TYPE LPAREN x=ID COLON TYPE DOUBLE_ARROW body=term RPAREN
  { let loc = L.make $startpos $endpos in
    E.uforall_type ~loc (Var.of_string x) body }
| EX_TYPE LPAREN x=ID COLON TYPE DOUBLE_ARROW body=term RPAREN
  { let loc = L.make $startpos $endpos in
    E.uexists_type ~loc (Var.of_string x) body }
| EQUAL ty=type_simple t=term_simple u=term_simple
  { let loc = L.make $startpos $endpos in
    E.uequal ~loc ty t u }
| LPAREN t=term RPAREN { t }
| x=ID COLON ty=typ DOUBLE_ARROW body=term_simple
  { let loc = L.make $startpos $endpos in
    E.ulambda ~loc (Var.of_string x) ty body }
| x=ID DEF t=term DOUBLE_ARROW u=term_simple
  { let loc = L.make $startpos $endpos in
    E.ulet ~loc (Var.of_string x) t u }
| CCARR a=type_simple b=type_simple
  { let loc = L.make $startpos $endpos in
    E.uarrow ~loc a b }

term:
| t=term_simple { t }
| f=term_simple l=term_simple+
  { E.uapp_list f l }
| error {
  let loc = L.make $startpos $endpos in
  dkparse_error ~loc "expected term"
}

type_simple:
| ty=qid
  { let loc = L.make $startpos $endpos in
    E.uvar ~loc ty }
| LPAREN ty=pre_typ RPAREN { ty }

pre_typ:
| f=type_simple l=type_simple*
  { E.uapp_list f l }
| CCARR a=type_simple b=type_simple
  { let loc = L.make $startpos $endpos in
    E.uarrow ~loc a b }

typ:
| TERM ty=type_simple { ty }
| PROP
  { let loc = L.make $startpos $endpos in
    E.uprop ~loc }

complex_type :
| ty=typ { ty }
| LPAREN ty=arrow_type RPAREN { ty }

arrow_type :
| ty=typ { ty }
| a=complex_type ARROW b=arrow_type
  { let loc = L.make $startpos $endpos in
    E.uarrow ~loc a b }
| x=ID COLON TYPE ARROW body=arrow_type
  { let loc = L.make $startpos $endpos in
    E.upi ~loc (Var.of_string x) (E.utype ~loc) body }

kind :
| TYPE { let loc = L.make $startpos $endpos in E.utype ~loc }
| ID COLON TYPE ARROW ty=kind
  { let loc = L.make $startpos $endpos in
    E.uarrow ~loc (E.utype ~loc) ty }

declared_or_defined_id:
| x=ID { x }
| x=QID { x }
compact_arg:
| LPAREN id=ID COLON ty=arrow_type RPAREN { Var.of_string id, ty }

type_const:
| TYPE { let loc = L.make $startpos $endpos in E.utype ~loc }

env_decl:
| id=ID COLON ty=arrow_type { Var.of_string id, Some ty }
| id=ID COLON t=type_const { Var.of_string id, Some t }
| id=ID { Var.of_string id, None }

env:
| LBRACK l=separated_list(COMMA, env_decl) RBRACK { l }


%%
