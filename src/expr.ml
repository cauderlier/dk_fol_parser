type builtin =
  | True | False | Not | And | Or | Imp | Eqv | Equal
  | Nat | Prop | Type | Kind

type binder =
  | Lam | All | Ex | AllType | ExType | Pi

type 'a node = { view : 'a; loc : Location.t; ty : typed expr option }
and 'typed expr_view =
  | Var of Var.t
  | Num of string
  | Builtin of builtin
  | Let of Var.t * 'typed expr * 'typed expr
  | App of 'typed expr * 'typed expr
  | Bind of binder * Var.t * 'typed expr * 'typed expr
  | Arrow of 'typed expr * 'typed expr
  | Alias of Var.t * 'typed expr
and 'typed expr = 'typed expr_view node
and typed

type untyped
type 'typed t = 'typed expr
type uexpr = untyped expr
type texpr = typed expr

let view e = e.view
let get_loc (e : 'typed expr) = e.loc
let get_ty (e : typed expr) =
  match e.ty with
  | None -> invalid_arg "Expr.get_ty"
  | Some ty -> ty

let rename_var x y z =
  if Var.eq x z then y else if Var.eq y z then x else z

let rec rename_expr : type t. Var.t -> Var.t -> t expr -> t expr = fun x y a ->
  if Var.eq x y then a else
    begin
      let loc = a.loc in
      let ty : typed expr option = rename_opt x y a.ty in
      match a.view with
      | Var z -> { view = Var (rename_var x y z); loc; ty }
      | Num _ | Builtin _ -> a
      | Let (z, v, b) ->
         { view = Let (rename_var x y z,
                       rename_expr x y v,
                       rename_expr x y b);
           loc; ty }
      | App (f, a) ->
         { view = App (rename_expr x y f,
                       rename_expr x y a);
           loc; ty }
      | Bind (b, z, t, body) ->
         { view = Bind (b,
                        rename_var x y z,
                        rename_expr x y t,
                        rename_expr x y body);
           loc; ty }
      | Arrow (a, b) ->
         { view = Arrow (rename_expr x y a,
                         rename_expr x y b);
           loc; ty }
      | Alias (z, t) ->
         { view = Alias (rename_var x y z,
                         rename_expr x y t);
           loc; ty }
    end

and rename_opt : type t. Var.t -> Var.t -> t expr option -> t expr option =
  fun x y -> function
  | None -> None
  | Some t -> Some (rename_expr x y t)

(* alpha_equivalence *)
let rec equal : type t. t expr -> t expr -> bool = fun a b ->
  a == b ||
    match a.view, b.view with
    | Var x, Var y -> Var.eq x y
    | Num x, Num y -> x = y
    | Builtin x, Builtin y -> x = y
    | Let (xa, va, ba), Let (xb, vb, bb) ->
       if Var.eq xa xb then
         equal va vb && equal ba bb
       else
         equal va (rename_expr xa xb vb) && equal ba (rename_expr xa xb bb)
    | App (fa, ta), App (fb, tb) ->
       equal fa fb && equal ta tb
    | Bind (bda, xa, ta, ba), Bind (bdb, xb, tb, bb) ->
       if Var.eq xa xb then
         bda = bdb && equal ta tb && equal ba bb
       else
         bda = bdb && equal ta (rename_expr xa xb tb) &&
           equal ba (rename_expr xa xb bb)
    | Arrow (ta, ua), Arrow (tb, ub) ->
       equal ta tb && equal ua ub
    | Alias (xa, ta), Alias (xb, tb) ->
       if Var.eq xa xb then
         equal ta tb
       else
         equal ta (rename_expr xa xb tb)
    | Alias (_, ta), _ ->
       equal ta b
    | _, Alias (_, tb) ->
       equal a tb
    | _ -> false

let rec free_vars t = match view t with
  | Var x -> Varset.singl x
  | Num _ | Builtin _ -> Varset.empty
  | App (f, a) ->
     Varset.union (free_vars f) (free_vars a)
  | Let (x, a, b) | Bind (_, x, a, b) ->
     Varset.union (free_vars a) (Varset.remove x (free_vars b))
  | Arrow (a, b) -> Varset.union (free_vars a) (free_vars b)
  | Alias (_, ty) -> free_vars ty

let rec subst x a e =
  let loc = e.loc in
  let ty = subst_opt x a e.ty in
  match e.view with
  | Var y when Var.eq x y ->
     assert (match ty with None -> false | Some ty -> equal ty (get_ty a));
     a
  | (Var _ | Num _ | Builtin _) as view ->
     { view; loc; ty }
  | Let (y, v, b) ->
     let capture_list = x :: Varset.to_list (free_vars a) in
     let z = Var.fresh y capture_list in
     { view = Let (z,
                   subst x a (rename_expr y z v),
                   subst x a (rename_expr y z b));
       loc; ty }
  | App (f, b) ->
     { view = App (subst x a f, subst x a b); loc; ty }
  | Bind (bd, y, t, b) ->
     let capture_list = x :: Varset.to_list (free_vars a) in
     let z = Var.fresh y capture_list in
     { view = Bind (bd,
                    z,
                    subst x a (rename_expr y z t),
                    subst x a (rename_expr y z b));
       loc; ty }
  | Arrow (t, u) ->
     { view = Arrow (subst x a t, subst x a u); loc; ty }
  | Alias (y, t) ->
     { view = Alias (y, subst x a t); loc; ty }
and subst_opt x a = function
  | None -> None
  | Some t -> Some (subst x a t)

let tmk_ ~loc ~ty view = { view; loc; ty = Some ty }
let umk_ ~loc view = { view; loc; ty = None }

(* The ony typed expr for which the ty field is None *)
let kind_ =
  { view = Builtin Kind; loc = Location.dummy; ty = None }
let tkind : texpr = kind_
let ukind : uexpr = kind_

let ubuiltin ~loc b = umk_ ~loc (Builtin b)
(* The type of a ubuiltin is known in advance but we cannot yet express all of them *)
let tbuiltin_aux ~loc ~ty b = tmk_ ~loc ~ty (Builtin b)

let utype ~loc = ubuiltin ~loc Type
let ttype ~loc = tbuiltin_aux ~loc ~ty:tkind Type

let uvar ~loc s = umk_ ~loc (Var s)
let tvar ~loc ~ty s = tmk_ ~loc ~ty (Var s)

let uarrow ~loc a b = umk_ ~loc (Arrow (a, b))
let tarrow ~loc a b =
  let ty = get_ty b in
  tmk_ ~loc ~ty (Arrow (a, b))

let ubind ~loc b x ty t  = umk_ ~loc (Bind (b, x, ty, t))
let rec tbind ~loc binder x t body =
  let tb = get_ty body in
  let ty =
    match binder with
    | Lam ->
    (* Lambda is the only binder for which the type of the expression
       differs from the type of the body *)
       if Varset.mem x (free_vars tb) then
         tpi ~loc x t tb
       else
         tarrow ~loc t tb
    | _ -> tb
  in
  tmk_ ~loc ~ty (Bind (binder, x, t, body))

and tpi ~loc = tbind ~loc Pi
let upi ~loc = ubind ~loc Pi

let uprop ~loc = ubuiltin ~loc Prop
let tprop ~loc = tbuiltin_aux ~loc ~ty:(ttype ~loc) Prop

(* The type of a ubuiltin *)
let builtin_type b =
  let loc = Location.dummy in
  let tarr a b = tarrow ~loc a b in
  let type_ = ttype ~loc in
  let prop = tprop ~loc in
  let prop2 = tarr prop prop in
  let prop3 = tarr prop prop2 in
  let alpha = Var.of_string "α" in
  let a = tvar ~loc ~ty:type_ alpha in
  match b with
  | True | False -> prop
  | Not -> prop2
  | And | Or | Imp | Eqv -> prop3
  | Equal -> tpi ~loc alpha type_ (tarr a (tarr a prop))
  | Nat | Prop -> type_
  | Type -> tkind
  | Kind -> failwith "builtin_type"

let tbuiltin ~loc b =
  match b with
  | Kind -> tkind
  | b ->
     let ty = builtin_type b in
     tbuiltin_aux ~loc ~ty b

let unat ~loc = ubuiltin ~loc Nat
let tnat ~loc = tbuiltin ~loc Nat

let unum ~loc n = umk_ ~loc (Num n)
let tnum ~loc n = tmk_ ~loc ~ty:(tnat ~loc) (Num n)

let ulet ~loc x a b = umk_ ~loc (Let (x, a, b))
let tlet ~loc x a b =
  let ty = get_ty b in
  tmk_ ~loc ~ty (Let (x, a, b))

let utrue ~loc = ubuiltin ~loc True
let ttrue ~loc = tbuiltin ~loc True

let ufalse ~loc = ubuiltin ~loc False
let tfalse ~loc = tbuiltin ~loc False

let ulambda ~loc x ty t = ubind ~loc Lam x ty t
let tlambda ~loc x ty t = tbind ~loc Lam x ty t

let uforall ~loc x ty t = ubind ~loc All x ty t
let uexists ~loc x ty t = ubind ~loc Ex x ty t
let uforall_type ~loc x t = ubind ~loc AllType x (utype ~loc) t
let uexists_type ~loc x t = ubind ~loc Ex x (utype ~loc) t
let ualias ~loc x ty = umk_ ~loc (Alias (x, ty))
let talias ~loc x ty = tmk_ ~loc ~ty:(ttype ~loc) (Alias (x, ty))

type type_mismatch = { tm_loc : Location.t; tm_term : texpr; expected : texpr; infered : texpr}
exception TypeMismatch of type_mismatch

type not_an_arrow =
  { nar_loc : Location.t; nar_term : texpr option; nar_type : texpr}
exception NotAnArrow of not_an_arrow

let uapp f a =
  let loc = Location.combine f.loc a.loc in
  umk_ ~loc (App (f, a))

let get_domain (f : texpr) =
  let loc = get_loc f in
  let ty = get_ty f in
  match view ty with
  | Arrow (tya, _) | Bind (Pi, _, tya, _) -> tya
  | _ -> raise (NotAnArrow {nar_loc = loc; nar_term = Some f; nar_type = ty})

let get_type_range ~loc f (ty : texpr) (a : texpr) =
  match view ty with
  | Arrow (_, tyb) -> tyb
  | Bind (Pi, x, _, tyb) -> subst x a tyb
  | _ -> raise (NotAnArrow {nar_loc = loc; nar_term = f; nar_type = ty})

let get_range ~loc f a = get_type_range ~loc (Some f) (get_ty f) a

let tapp ~loc f a =
  let tya = get_ty a in
  let tya' = get_domain f in
  if not (equal tya tya') then
    raise (TypeMismatch {tm_loc=a.loc; tm_term = a; expected = tya'; infered = tya});
  tmk_ ~loc ~ty:(get_range ~loc f a) (App (f, a))

let rec uapp_list f = function
  | [] -> f
  | a :: l -> uapp_list (uapp f a) l

let unot ~loc a = uapp (ubuiltin ~loc Not) a
let uand ~loc a b = uapp_list (ubuiltin ~loc And) [a; b]
let uor ~loc a b = uapp_list (ubuiltin ~loc Or) [a; b]
let uimply ~loc a b = uapp_list (ubuiltin ~loc Imp) [a; b]
let uequiv ~loc a b = uapp_list (ubuiltin ~loc Eqv) [a; b]
let uequal ~loc ty a b = uapp_list (ubuiltin ~loc Equal) [ty; a; b]

let rec forget_types t =
  let loc = t.loc in
  match t.view with
  | Var s -> uvar ~loc s
  | Num s -> unum ~loc s
  | Builtin Type -> utype ~loc
  | Builtin b -> ubuiltin ~loc b
  | App (f, a) -> uapp (forget_types f) (forget_types a)
  | Let (x, a, b) -> ulet ~loc x (forget_types a) (forget_types b)
  | Bind (b, x, ty, t) -> ubind ~loc b x (forget_types ty) (forget_types t)
  | Alias (x, ty) -> ualias ~loc x (forget_types ty)
  | Arrow (a, b) -> uarrow ~loc (forget_types a) (forget_types b)


(* Used to compute the type of a defined symbol. *)
let rec ty_with_params ~loc args ty = match args with
  | [] -> ty
  | (x, tyx) :: args ->
     let rest = ty_with_params ~loc args ty in
     if Varset.mem x (free_vars rest) then
       upi ~loc x tyx rest
     else
       uarrow ~loc tyx rest

(* Used to compute the type of a defined symbol. *)
let rec typed_ty_with_params ~loc args ty = match args with
  | [] -> ty
  | (x, tyx) :: args ->
     let rest = typed_ty_with_params ~loc args ty in
     if Varset.mem x (free_vars rest) then
       tpi ~loc x tyx rest
     else
       tarrow ~loc tyx rest
