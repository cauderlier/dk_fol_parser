module E = Expr
module S = Statement

let builtin_to_string = function
  | E.True -> "dk_logic.true"
  | E.False -> "dk_logic.false"
  | E.Not -> "dk_logic.not"
  | E.And -> "dk_logic.and"
  | E.Or -> "dk_logic.or"
  | E.Imp -> "dk_logic.imp"
  | E.Eqv -> "dk_logic.eqv"
  | E.Equal -> "dk_logic.equal"
  | E.Nat -> "'dk_nat.nat'"
  | E.Prop -> "dk_builtins.prop"
  | E.Type -> "cc.uT"
  | E.Kind -> "Kind"

let binder_to_string = function
  | E.Lam -> ""
  | E.All -> "dk_logic.forall"
  | E.Ex -> "dk_logic.exists"
  | E.AllType -> "dk_logic.forall_type"
  | E.ExType -> "dk_logic.exists_type"
  | E.Pi -> ""

let pprint_var fmt x = Format.fprintf fmt "%s" (Var.to_string x)

let rec pprint_expr fmt e = match E.view e with
  | E.Var x -> Format.fprintf fmt "%a" pprint_var x
  | E.Num n -> Format.fprintf fmt "%s" n
  | E.Builtin b ->
     Format.fprintf fmt "%s" (builtin_to_string b)
  | E.Let (x, a, b) ->
     Format.fprintf fmt "@[(%a :=@ %a@ =>@ %a)@]"
       pprint_var x pprint_expr a pprint_expr b
  | E.App (a, b) ->
     Format.fprintf fmt "@[(%a@ %a)@]"
       pprint_expr a pprint_expr b
  | E.Bind (E.Lam, x, ty, t) ->
     Format.fprintf fmt "@[(%a : %a =>@ %a)@]"
       pprint_var x
       pprint_type ty
       pprint_expr t
  | E.Bind (E.Pi, x, ty, t) ->
     Format.fprintf fmt "@[(%a : %a ->@ %a)@]"
       pprint_var x
       pprint_type ty
       pprint_type t
  | E.Bind (b, x, ty, t) ->
     Format.fprintf fmt "@[<1>(%s %a (%a : %a =>@ %a))@]"
       (binder_to_string b)
       pprint_expr ty
       pprint_var x
       pprint_type ty
       pprint_expr t
  | E.Arrow (a, b) ->
     Format.fprintf fmt "@[(cc.Arrow %a@ %a)@]"
       pprint_expr a
       pprint_expr b
  | E.Alias (x, _) -> pprint_var fmt x

and pprint_type fmt e = match E.view e with
  | E.Builtin E.Prop -> Format.fprintf fmt "dk_logic.Prop"
  | E.Arrow (a, b) ->
     Format.fprintf fmt "@[(%a ->@ %a)@]"
       pprint_type a
       pprint_type b
  | E.Builtin E.Type | E.Bind (E.Pi, _, _, _) -> pprint_expr fmt e
  | _ -> Format.fprintf fmt "@[(cc.eT %a)@]" pprint_expr e

let print_arg fmt (x, ty) =
  Format.fprintf fmt "(%a : %a)@ " pprint_var x pprint_type ty

let print_context_decl fmt (x, ty_opt) =
  match ty_opt with
  | None -> pprint_var fmt x
  | Some ty -> Format.fprintf fmt "%a : %a" pprint_var x pprint_type ty

let pprint_statement fmt s = match S.view s with
  | S.TypeAlias (x, ty) ->
     Format.fprintf fmt "@[<2>def (; type alias ;) %a : cc.uT :=@ %a.@]@."
       pprint_var x pprint_expr ty
  | S.Decl (x, ty, S.Hyp) ->
     Format.fprintf fmt "@[<2>(; hypothesis ;) %a :@ dk_logic.eP %a.@]@."
       pprint_var x pprint_expr ty
  | S.Decl (x, ty, _) ->
     Format.fprintf fmt "@[<2>(; declaration ;) %a :@ %a.@]@."
       pprint_var x pprint_type ty
  | S.Def (x, bindings, None, body) ->
     Format.fprintf fmt "@[<2>def %a@ @[%a@]:=@ %a.@]@."
       pprint_var x (Utils.print_list print_arg) bindings
       pprint_expr body
  | S.Def (x, bindings, Some ty, body) ->
     Format.fprintf fmt "@[<2>def %a@ @[%a@]: %a@ :=@ %a.@]@."
       pprint_var x (Utils.print_list print_arg) bindings
       pprint_type ty pprint_expr body
  | S.Rew (bindings, lhs, rhs) ->
     Format.fprintf fmt "@[<2>[%a]@ %a@ -->@ %a.@]@."
       (Utils.print_list_with_commas print_context_decl) bindings
       pprint_expr lhs pprint_expr rhs
  | S.Goal (g, f) ->
     Format.fprintf fmt "@[<2>(; goal ;) %a :@ dk_logic.eP %a.@]@."
       pprint_var g pprint_expr f

let pprint_statements fmt = List.iter (pprint_statement fmt)
